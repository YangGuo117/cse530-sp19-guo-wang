package hw1;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

/**
 * This class represents a tuple that will contain a single row's worth of information
 * from a table. It also includes information about where it is stored
 * @author Sam Madden modified by Doug Shook
 *
 */
public class Tuple {
	private TupleDesc td;
	private int pid;
	private int id;
//	private byte[] data;
	private Field[] field;
	/**
	 * Creates a new tuple with the given description
	 * @param t the schema for this tuple
	 */
	public Tuple(TupleDesc t) {
		this.td = t;
//		this.data = new byte[t.getSize()];
		this.field = new Field[t.numFields()];
		//your code here
	}
	
	public TupleDesc getDesc() {
		//your code here
		return this.td;
	}
	
	/**
	 * retrieves the page id where this tuple is stored
	 * @return the page id of this tuple
	 */
	public int getPid() {
		//your code here
		return this.pid;
	}

	public void setPid(int pid) {
		//your code here
		this.pid = pid;
	}

	/**
	 * retrieves the tuple (slot) id of this tuple
	 * @return the slot where this tuple is stored
	 */
	public int getId() {
		//your code here
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
		//your code here
	}
	
	public void setDesc(TupleDesc td) {
		this.td = td;
		//your code here;
	}
	
	/**
	 * Stores the given data at the i-th field
	 * @param i the field number to store the data
	 * @param v the data
	 */
	public void setField(int i, Field v) {
//		int index_lo = this.getIndex(i);
//		int index_up = (v.getType().equals(Type.INT)) ? 4 + index_lo : 129 + index_lo;
//		byte[] data = v.toByteArray();
//		for (int j = index_lo; j < index_up; j++) {
//			this.data[j] = data[j-index_lo];
//		}
		this.field[i] = v;
		//your code here
	}
	
	public Field getField(int i) {
//		int index_lo = this.getIndex(i);
//		int index_up = (this.td.getType(i).equals(Type.INT)) ? 4 + index_lo : 129 + index_lo;
//		byte[] data = Arrays.copyOfRange(this.data, index_lo, index_up);
//		if (this.td.getType(i).equals(Type.INT)) return new IntField(data);
//		else return new StringField(data);
		return this.field[i];
	}
//	private int getIndex(int ii) {
//		int index = 0;
//		for (int i = 0; i < ii; i++) {
//			index += (this.td.getType(i).equals(Type.INT)) ? 4 : 129;
//		}
//		return index;
//	}
	/**
	 * Creates a string representation of this tuple that displays its contents.
	 * You should convert the binary data into a readable format (i.e. display the ints in base-10 and convert
	 * the String columns to readable text).
	 */
	public String toString() {
		String str = "";
    	for (int i = 0; i < this.td.numFields(); i++) {
    		String dot = (i == this.td.numFields() - 1) ? "" : ",";
    		String content = this.field[i].toString();
//    		byte[] data = this.field[i].toByteArray();
//    		if (this.td.getType(i).equals(Type.STRING)) {
//    			int length = data[0];
//    			byte[] temp = Arrays.copyOfRange(data, 1, 1 + length);
//    			content = new String(temp);
//    		} else {
//    			ByteBuffer wrapped = ByteBuffer.wrap(data);
//    			int temp = wrapped.getInt();
//    			content = Integer.toString(temp);
//    		}
    		str += content + dot;
    		//System.out.println(content);
    	}
        //your code here
    	return str;
	}
}
	