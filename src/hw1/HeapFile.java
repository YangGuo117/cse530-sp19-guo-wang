package hw1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * A heap file stores a collection of tuples. It is also responsible for managing pages.
 * It needs to be able to manage page creation as well as correctly manipulating pages
 * when tuples are added or deleted.
 * @author Sam Madden modified by Doug Shook
 *
 */
public class HeapFile {
	
	public static final int PAGE_SIZE = 4096; // 4kb
	private TupleDesc tp;
	private File file;
	/**
	 * Creates a new heap file in the given location that can accept tuples of the given type
	 * @param f location of the heap file
	 * @param types type of tuples contained in the file
	 */
	public HeapFile(File f, TupleDesc type) {
		this.tp = type;
		this.file = f;
		//your code here
	}
	
	public File getFile() {
		//your code here
		return this.file;
	}
	
	public TupleDesc getTupleDesc() {
		//your code here
		return this.tp;
	}
	
	/**
	 * Creates a HeapPage object representing the page at the given page number.
	 * Because it will be necessary to arbitrarily move around the file, a RandomAccessFile object
	 * should be used here.
	 * @param id the page number to be retrieved
	 * @return a HeapPage at the given page number
	 */
	public HeapPage readPage(int id) {
		byte[] data = new byte[PAGE_SIZE];
		try {
			RandomAccessFile raf = new RandomAccessFile (file,"r");
			raf.seek((long)(id * PAGE_SIZE));
			raf.read(data);
			raf.close();
			return new HeapPage(id, data, getId());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		//your code here
		return null;
	}
	
	/**
	 * Returns a unique id number for this heap file. Consider using
	 * the hash of the File itself.
	 * @return
	 */
	public int getId() {
		//your code here
		return this.file.hashCode();
	}
	
	/**
	 * Writes the given HeapPage to disk. Because of the need to seek through the file,
	 * a RandomAccessFile object should be used in this method.
	 * @param p the page to write to disk
	 */
	public void writePage(HeapPage p) {
		byte[] data = p.getPageData();
		int id = p.getId();
		try {
			RandomAccessFile raf = new RandomAccessFile (file,"rw");
			raf.seek((long)(id * PAGE_SIZE));
			raf.write(data);
			raf.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		//your code here
	}
	
	/**
	 * Adds a tuple. This method must first find a page with an open slot, creating a new page
	 * if all others are full. It then passes the tuple to this page to be stored. It then writes
	 * the page to disk (see writePage)
	 * @param t The tuple to be stored
	 * @return The HeapPage that contains the tuple
	 */
	public HeapPage addTuple(Tuple t) {
		for (int i=0; i < this.getNumPages(); i++) {
			HeapPage hp = readPage(i);
			int numSlot = hp.getNumSlots();
			int j = 0;
			while (j < numSlot && hp.slotOccupied(j)) j+=1;
			if (j >= numSlot) continue;
			else {
				try {
//					System.out.println(hp.slotOccupied(1));
					hp.addTuple(t);
					writePage(hp);
					return hp;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		try {
			HeapPage hp_new = new HeapPage(getNumPages(), new byte[PAGE_SIZE], getId());
			hp_new.addTuple(t);
			writePage(hp_new);
			return hp_new;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//your code here
		return null;
	}
	// for buffer pool
	public int getInsertHpId() {
		for (int i=0; i < this.getNumPages(); i++) {
			HeapPage hp = readPage(i);
			int numSlot = hp.getNumSlots();
			int j = 0;
			while (j < numSlot && hp.slotOccupied(j)) j+=1;
			if (j >= numSlot) continue;
			else {
				return i;
			}
		}
		return -1;
	}
	
	/**
	 * This method will examine the tuple to find out where it is stored, then delete it
	 * from the proper HeapPage. It then writes the modified page to disk.
	 * @param t the Tuple to be deleted
	 */
	public void deleteTuple(Tuple t){
		int pid = t.getPid();
		HeapPage hp = readPage(pid);
		try {
			hp.deleteTuple(t);
			this.writePage(hp);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//your code here
	}
	
	/**
	 * Returns an ArrayList containing all of the tuples in this HeapFile. It must
	 * access each HeapPage to do this (see iterator() in HeapPage)
	 * @return
	 */
	public ArrayList<Tuple> getAllTuples() {
		ArrayList<Tuple> tps = new ArrayList<>();
		for (int i = 0; i < this.getNumPages(); i++) {
			Iterator<Tuple> itr = readPage(i).iterator();
			while (itr.hasNext()) tps.add(itr.next());
		}
		//your code here
		return tps;
	}
	
	/**
	 * Computes and returns the total number of pages contained in this HeapFile
	 * @return the number of pages
	 */
	public int getNumPages() {
		//your code here
		return (int) Math.ceil(1.0 * this.file.length() / PAGE_SIZE);
	}
}
