package hw1;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.NoSuchElementException;

import org.junit.experimental.theories.Theories;


public class HeapPage {

	private int id;
	private byte[] header;
	private Tuple[] tuples;
	private TupleDesc td;
	private int numSlots;
	private int tableId;



	public HeapPage(int id, byte[] data, int tableId) throws IOException {
		this.id = id;
		this.tableId = tableId;

		this.td = Database.getCatalog().getTupleDesc(this.tableId);
		this.numSlots = getNumSlots();
		DataInputStream dis = new DataInputStream(new ByteArrayInputStream(data));

		// allocate and read the header slots of this page
		header = new byte[getHeaderSize()];
		for (int i=0; i<header.length; i++)
			header[i] = dis.readByte();

		try{
			// allocate and read the actual records of this page
			tuples = new Tuple[numSlots];
			for (int i=0; i<tuples.length; i++)
				tuples[i] = readNextTuple(dis,i);
		}catch(NoSuchElementException e){
			e.printStackTrace();
		}
		dis.close();
	}

	public int getId() {
		//your code here
		return id;
	}

	/**
	 * Computes and returns the total number of slots that are on this page (occupied or not).
	 * Must take the header into account!
	 * @return number of slots on this page
	 */
	public int getNumSlots() {
		int slot_size = this.td.getSize();
		int slotNum = 4096 / slot_size;
		int headerSize = (int) Math.ceil(1.0 * slotNum / 8);
		int spaceLeft = 4096 - slotNum * slot_size;
		while (spaceLeft < headerSize) {
			slotNum -= 1;
			headerSize = (int) Math.ceil(1.0 * slotNum / 8);
			spaceLeft = 4096 - slotNum * slot_size;
		}
		//your code here
		return slotNum;
	}

	/**
	 * Computes the size of the header. Headers must be a whole number of bytes (no partial bytes)
	 * @return size of header in bytes
	 */
	private int getHeaderSize() {        
		//your code here
		return (int) Math.ceil(1.0 * this.numSlots / 8);
	}

	/**
	 * Checks to see if a slot is occupied or not by checking the header
	 * @param s the slot to test
	 * @return true if occupied
	 */
	public boolean slotOccupied(int s) {
		int h_index = s / 8;
		int bit_index = s % 8;
		byte the_head = header[h_index];
		int slot_status = (byte)((the_head >> bit_index) & 1);
//		System.out.println(h_index);
//		System.out.println(byteToBit(the_head));
		//your code here
		return slot_status == 1;
	}
	
	public static String byteToBit(byte b) {  
	    return "" +(byte)((b >> 7) & 0x1) +   
	    (byte)((b >> 6) & 0x1) +   
	    (byte)((b >> 5) & 0x1) +   
	    (byte)((b >> 4) & 0x1) +   
	    (byte)((b >> 3) & 0x1) +   
	    (byte)((b >> 2) & 0x1) +   
	    (byte)((b >> 1) & 0x1) +   
	    (byte)((b >> 0) & 0x1);  
	}  

	/**
	 * Sets the occupied status of a slot by modifying the header
	 * @param s the slot to modify
	 * @param value its occupied status
	 */
	public void setSlotOccupied(int s, boolean value) {
		int h_index = s / 8;
		int bit_index = s % 8;
		int mask_p = (int) Math.pow(2,bit_index);
		int mask_n = (int) Math.pow(2,8) - 1 - mask_p;
		byte the_head = header[h_index];
		byte new_head;
//		System.out.println(Integer.toString(s)+" " + Boolean.toString(value));
//		System.out.println("before "+  Byte.toString(the_head));
//		System.out.println("T "+  Integer.toBinaryString(mask_p));
//		System.out.println("F "+  Integer.toBinaryString(mask_n));
		if (value) new_head = (byte) (the_head | mask_p);
		else new_head = (byte) (the_head & mask_n);
//		System.out.println("after "+  Byte.toString(new_head));
		this.header[h_index] = new_head;
//		System.out.println(h_index);
//		System.out.println(byteToBit(this.header[h_index]));
		//your code here
	}
	
	/**
	 * Adds the given tuple in the next available slot. Throws an exception if no empty slots are available.
	 * Also throws an exception if the given tuple does not have the same structure as the tuples within the page.
	 * @param t the tuple to be added.
	 * @throws Exception
	 */
	public void addTuple(Tuple t) throws Exception {
		if (!t.getDesc().equals(td)) throw new Exception("given tuple does not have proper structure");
		int i = 0;
		while (i < this.numSlots && this.slotOccupied(i)) i += 1;
		if (i >= this.numSlots) throw new Exception("no empety slots avaliable");
		this.tuples[i] = t;
		t.setPid(this.id);
		t.setId(i);
		this.setSlotOccupied(i,true);
		//your code here
	}
	
	// 可以改为 heapmap 如果 没过test的话
	private HashMap<Integer, ArrayList<Tuple>> insertMap = new HashMap<Integer, ArrayList<Tuple>>();
	private HashMap<Integer, ArrayList<Tuple>> deleteMap = new HashMap<Integer, ArrayList<Tuple>>();
	
	public void tryInsert(Tuple t, int tsId) {
		if (insertMap.containsKey(tsId)) {
			ArrayList<Tuple> tempTuples = insertMap.get(tsId);
			tempTuples.add(t);
		} else {
			ArrayList<Tuple> tempTuples = new ArrayList<Tuple>();
			tempTuples.add(t);
			insertMap.put(tsId, tempTuples);
		}
	}
	public void tryDelete(Tuple t, int tsId) {
		if (deleteMap.containsKey(tsId)) {
			ArrayList<Tuple> tempTuples = deleteMap.get(tsId);
			tempTuples.add(t);
		} else {
			ArrayList<Tuple> tempTuples = new ArrayList<Tuple>();
			tempTuples.add(t);
			deleteMap.put(tsId, tempTuples);
		}
	}
	public void commit(int tsId) {
		if (insertMap.containsKey(tsId)) {
			ArrayList<Tuple> tempTuples = insertMap.get(tsId);
			for (Tuple t : tempTuples) {
				try {
					this.addTuple(t);
				} catch (Exception e) {}
			}
			insertMap.remove(tsId);
		}
		if (deleteMap.containsKey(tsId)) {
			ArrayList<Tuple> tempTuples = deleteMap.get(tsId);
			for (Tuple t : tempTuples) {
				try {
					this.deleteTuple(t);
				} catch (Exception e) {}
			}
			deleteMap.remove(tsId);
		}
	}
	public void abort(int tsId) {
		if (insertMap.containsKey(tsId)) {
			insertMap.remove(tsId);
		}
		if (deleteMap.containsKey(tsId)) {
			deleteMap.remove(tsId);
		}
	}
	public Boolean isDirty() {
		return !insertMap.isEmpty() || !deleteMap.isEmpty();
	}

	/**
	 * Removes the given Tuple from the page. If the page id from the tuple does not match this page, throw
	 * an exception. If the tuple slot is already empty, throw an exception
	 * @param t the tuple to be deleted
	 * @throws Exception
	 */
	public void deleteTuple(Tuple t) throws Exception  {
		if (t.getPid() != this.id) throw new Exception("Tuple does not match this page ");
		int slotId = t.getId();
		if (!this.slotOccupied(slotId)) throw new Exception("The slot is already empety");
		this.tuples[slotId] = null;
		this.setSlotOccupied(slotId,false);
		//your code here
	}
	
	/**
     * Suck up tuples from the source file.
     */
	private Tuple readNextTuple(DataInputStream dis, int slotId) {
		// if associated bit is not set, read forward to the next tuple, and
		// return null.
		if (!slotOccupied(slotId)) {
			for (int i=0; i<td.getSize(); i++) {
				try {
					dis.readByte();
				} catch (IOException e) {
					throw new NoSuchElementException("error reading empty tuple");
				}
			}
			return null;
		}

		// read fields in the tuple
		Tuple t = new Tuple(td);
		t.setPid(this.id);
		t.setId(slotId);

		for (int j=0; j<td.numFields(); j++) {
			if(td.getType(j) == Type.INT) {
				byte[] field = new byte[4];
				try {
					dis.read(field);
					t.setField(j, new IntField(field));
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				byte[] field = new byte[129];
				try {
					dis.read(field);
					t.setField(j, new StringField(field));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}


		return t;
	}

	/**
     * Generates a byte array representing the contents of this page.
     * Used to serialize this page to disk.
	 *
     * The invariant here is that it should be possible to pass the byte
     * array generated by getPageData to the HeapPage constructor and
     * have it produce an identical HeapPage object.
     *
     * @return A byte array correspond to the bytes of this page.
     */
	public byte[] getPageData() {
		int len = HeapFile.PAGE_SIZE;
		ByteArrayOutputStream baos = new ByteArrayOutputStream(len);
		DataOutputStream dos = new DataOutputStream(baos);

		// create the header of the page
		for (int i=0; i<header.length; i++) {
			try {
				dos.writeByte(header[i]);
			} catch (IOException e) {
				// this really shouldn't happen
				e.printStackTrace();
			}
		}

		// create the tuples
		for (int i=0; i<tuples.length; i++) {

			// empty slot
			if (!slotOccupied(i)) {
				for (int j=0; j<td.getSize(); j++) {
					try {
						dos.writeByte(0);
					} catch (IOException e) {
						e.printStackTrace();
					}

				}
				continue;
			}

			// non-empty slot
			for (int j=0; j<td.numFields(); j++) {
				Field f = tuples[i].getField(j);
				try {
					dos.write(f.toByteArray());

				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		// padding
		int zerolen = HeapFile.PAGE_SIZE - (header.length + td.getSize() * tuples.length); //- numSlots * td.getSize();
		byte[] zeroes = new byte[zerolen];
		try {
			dos.write(zeroes, 0, zerolen);
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			dos.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return baos.toByteArray();
	}

	/**
	 * Returns an iterator that can be used to access all tuples on this page. 
	 * @return
	 */
	public Iterator<Tuple> iterator() {
		//your code here
		return this.new EvenIterator();
	}
	interface Hp_Iterator extends java.util.Iterator<Tuple> { }
    
    private class EvenIterator implements Hp_Iterator {
    	private int nextSlotID = 0;
    	public EvenIterator() {
    		findNext();
    	}
    	@Override
		public boolean hasNext() {
//			System.out.println("has next "+  Integer.toString(nextSlotID));
			return (nextSlotID < numSlots);
		}
		private void findNext() {
			while (nextSlotID < numSlots && !slotOccupied(nextSlotID)) nextSlotID += 1;
		}
		@Override
		public Tuple next() {
			int ans = nextSlotID;
//			System.out.println("ans "+  Integer.toString(ans));
			nextSlotID += 1;
			findNext();
//			System.out.println("next "+  Integer.toString(nextSlotID));
			return tuples[ans];
		}
    	
    }
	
}
