package hw1;
import java.util.*;
import java.util.zip.DataFormatException;

/**
 * TupleDesc describes the schema of a tuple.
 */
public class TupleDesc {

	private Type[] types; // int str, fixed length, int 4 bite 32 bit, str uper bound 128 character + 5 header (chang du wei example 5hello)
	private String[] fields;
	
    /**
     * Create a new TupleDesc with typeAr.length fields with fields of the
     * specified types, with associated named fields.
     *
     * @param typeAr array specifying the number of and types of fields in
     *        this TupleDesc. It must contain at least one entry.
     * @param fieldAr array specifying the names of the fields. Note that names may be null.
     */
    public TupleDesc(Type[] typeAr, String[] fieldAr) {
    	this.types = typeAr;
    	this.fields = fieldAr;
    	//your code here
    }
    public Type[] getTypes() {
    	return this.types;
    }
    public String[] getFields() {
    	return this.fields;
    }
    /**
     * @return the number of fields in this TupleDesc
     */
    public int numFields() {
        //your code here
    	return fields.length;
    }

    /**
     * Gets the (possibly null) field name of the ith field of this TupleDesc.
     *
     * @param i index of the field name to return. It must be a valid index.
     * @return the name of the ith field
     * @throws NoSuchElementException if i is not a valid field reference.
     */
    public String getFieldName(int i) throws NoSuchElementException {
        //your code here
    	if (i>= 0 && i < this.fields.length) return this.fields[i];
    	else throw new NoSuchElementException("i is not a valid field reference");
    }

    /**
     * Find the index of the field with a given name.
     *
     * @param name name of the field.
     * @return the index of the field that is first to have the given name.
     * @throws NoSuchElementException if no field with a matching name is found.
     */
    public int nameToId(String name) throws NoSuchElementException {
    	for (int i = 0; i < this.types.length; i++) {
    		if (name.equals(this.fields[i])) return i;
    	}
    	throw new NoSuchElementException("no such name in field");
        //your code here
    }

    /**
     * Gets the type of the ith field of this TupleDesc.
     *
     * @param i The index of the field to get the type of. It must be a valid index.
     * @return the type of the ith field
     * @throws NoSuchElementException if i is not a valid field reference.
     */
    public Type getType(int i) throws NoSuchElementException {
    	if (i >= 0 && i < this.types.length) return this.types[i];
    	throw new NoSuchElementException("i is not a valid type reference");
        //your code here
    }

    /**
     * @return The size (in bytes) of tuples corresponding to this TupleDesc.
     * Note that tuples from a given TupleDesc are of a fixed size.
     */
    public int getSize() {
    	int size = 0;
    	for (Type type : this.types) {
    		if (type.equals(Type.INT)) size += 4;
    		if (type.equals(Type.STRING)) size += 129;
    	}
    	//your code here
    	// return turple size;
    	return size;
    }

    /**
     * Compares the specified object with this TupleDesc for equality.
     * Two TupleDescs are considered equal if they are the same size and if the
     * n-th type in this TupleDesc is equal to the n-th type in td.
     *
     * @param o the Object to be compared for equality with this TupleDesc.
     * @return true if the object is equal to this TupleDesc.
     */
    public boolean equals(Object o) {
    	TupleDesc td = (TupleDesc) o;
    	if (this.getSize() != td.getSize() ||this.numFields() != td.numFields()) return false;
    	for (int i = 0; i < this.types.length; i++) {
    		if (!this.types[i].equals(td.types[i])) return false;
    	}
    	//your code here
    	return true;
    }
    

    public int hashCode() {
        // If you want to use TupleDesc as keys for HashMap, implement this so
        // that equal objects have equals hashCode() results
        throw new UnsupportedOperationException("unimplemented");
    }

    /**
     * Returns a String describing this descriptor. It should be of the form
     * "fieldType[0](fieldName[0]), ..., fieldType[M](fieldName[M])", although
     * the exact format does not matter.
     * @return String describing this descriptor.
     */
    public String toString() {
    	String str = "";
    	for (int i = 0; i < this.types.length; i++) {
    		String dot = (i == this.types.length - 1) ? "" : ",";
    		String name = (this.fields[i] == null) ? "" : this.fields[i];
    		str += this.types[i].toString() + "(" + name + ")"+dot;
    	}
        //your code here
    	return str;
    }
}
