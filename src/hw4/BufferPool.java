package hw4;

import java.io.*;
import java.sql.Array;
import java.util.ArrayList;
import java.util.HashMap;

import hw1.Catalog;
import hw1.Database;
import hw1.HeapFile;
import hw1.HeapPage;
import hw1.Tuple;

/**
 * BufferPool manages the reading and writing of pages into memory from
 * disk. Access methods call into it to retrieve pages, and it fetches
 * pages from the appropriate location.
 * <p>
 * The BufferPool is also responsible for locking;  when a transaction fetches
 * a page, BufferPool which check that the transaction has the appropriate
 * locks to read/write the page.
 */
public class BufferPool {
    /** Bytes per page, including header. */
    public static final int PAGE_SIZE = 4096;

    /** Default number of pages passed to the constructor. This is used by
    other classes. BufferPool should use the numPages argument to the
    constructor instead. */
    public static final int DEFAULT_PAGES = 50;

    /**
     * Creates a BufferPool that caches up to numPages pages.
     *
     * @param numPages maximum number of pages in this buffer pool.
     */
    private pageInfo[] pageInfoArray;
    private HashMap<String,Integer> pageMap;
    private HashMap<Integer, ArrayList<Integer>> transMap; // transId : [related pages]
    
    
    public BufferPool(int numPages) {
        // your code here
    	this.pageInfoArray = new pageInfo[numPages];
    	this.pageMap = new HashMap<String,Integer>();
    	this.transMap = new HashMap<Integer, ArrayList<Integer>>();
    }

    /**
     * Retrieve the specified page with the associated permissions.
     * Will acquire a lock and may block if that lock is held by another
     * transaction.
     * <p>
     * The retrieved page should be looked up in the buffer pool.  If it
     * is present, it should be returned.  If it is not present, it should
     * be added to the buffer pool and returned.  If there is insufficient
     * space in the buffer pool, an page should be evicted and the new page
     * should be added in its place.
     *
     * @param tsId the ID of the transaction requesting the page
     * @param tableId the ID of the table with the requested page
     * @param pid the ID of the requested page
     * @param perm the requested permissions on the page
     */
    private int findSlot() {
    	// return the first empty slot
    	for (int i = 0; i < pageInfoArray.length; i++) {
    		if (pageInfoArray[i] == null) return i;
    	}
    	return -1; // full
    }
    private HeapPage getHp(int tableId, int pid) {
    	Catalog c = Database.getCatalog();
		HeapFile hf = c.getDbFile(tableId);
		HeapPage hp = hf.readPage(pid);
		return hp;
    }
    private HeapFile getHf(int tableId) {
    	Catalog c = Database.getCatalog();
		HeapFile hf = c.getDbFile(tableId);
		return hf;
    }
    
    public HeapPage getPage(int tsId, int tableId, int pid, Permissions perm)
        throws Exception {
    	String keyPair = "" + tableId + "," + pid;
    	// if page exist
		if (pageMap.containsKey(keyPair)) {
			// handle permission
			int i = pageMap.get(keyPair);
			pageInfo pInfo = pageInfoArray[i];
			HashMap<Integer, Permissions> tsMap = pInfo.tramsMap;
			if (pInfo.writeTrans >= 0) {
				// W + W/R
				if (pInfo.writeTrans != tsId) { // the writing trans is not the same with this one
					throw new Exception("W + W/R conflict");
				}
			} else { // no write lock
				// R + R
				if (perm == Permissions.READ_ONLY) tsMap.put(tsId, perm); 
				// R + W
				else if (tsMap.isEmpty()) { // no lock on page
					tsMap.put(tsId, perm);
					pInfo.writeTrans = tsId;
				} else if (tsMap.containsKey(tsId)) { // there exist read
					// the only read is it self
					if (tsMap.size() == 1) {
						tsMap.put(tsId, perm);
						pInfo.writeTrans = tsId;
					} else { // if exist other read, then abort this trans
						transactionComplete(tsId,false);
					}			
				} else throw new Exception("R + W conflict");
			}
			return pInfo.hp;

		} else { // page not exist
			//if no place
			if (findSlot() < 0) evictPage();
			
			// if has place
			if (findSlot() >= 0) {
				HeapPage hp = getHp(tableId, pid);
				pageInfo pInfo = new pageInfo(tsId, hp, perm);
				if (perm == Permissions.READ_WRITE) pInfo.writeTrans = tsId;
				int i = findSlot();
				pageInfoArray[i] = pInfo;
				pageMap.put(keyPair, i);
				return hp;
	    	} else throw new Exception("No place avaliable");
		} 	
        // your code here
    }

    /**
     * Releases the lock on a page.
     * Calling this is very risky, and may result in wrong behavior. Think hard
     * about who needs to call this and why, and why they can run the risk of
     * calling it.
     *
     * @param tsId the ID of the transaction requesting the unlock
     * @param tableID the ID of the table containing the page to unlock
     * @param pid the ID of the page to unlock
     */
    public  void releasePage(int tsId, int tableId, int pid) {
        // your code here
    	String keyPair = "" + tableId + "," + pid;
    	if (pageMap.containsKey(keyPair)) {
    		int i = pageMap.get(keyPair);
			pageInfo pInfo = pageInfoArray[i];
			HashMap<Integer, Permissions> tsMap = pInfo.tramsMap;
			if (tsMap.get(tsId) == Permissions.READ_WRITE) pInfo.writeTrans = -1;
			tsMap.remove(tsId);
    	}
    }

    /** Return true if the specified transaction has a lock on the specified page */
    public   boolean holdsLock(int tsId, int tableId, int pid) {
        // your code here
    	String keyPair = "" + tableId + "," + pid;
    	if (pageMap.containsKey(keyPair)) {
    		int i = pageMap.get(keyPair);
			pageInfo pInfo = pageInfoArray[i];
			HashMap<Integer, Permissions> tsMap = pInfo.tramsMap;
			return tsMap.containsKey(tsId);
    	}
        return false;
    }

    /**
     * Commit or abort a given transaction; release all locks associated to
     * the transaction. If the transaction wishes to commit, write
     *
     * @param tsId the ID of the transaction requesting the unlock
     * @param commit a flag indicating whether we should commit or abort
     */
    public   void transactionComplete(int tsId, boolean commit)
        throws IOException {
        // your code here
    	for (pageInfo pInfo: pageInfoArray) {
    		if (pInfo == null) continue;
    		HashMap<Integer, Permissions> tsMap = pInfo.tramsMap;
    		if (tsMap.containsKey(tsId)) {
    			if (tsMap.get(tsId) == Permissions.READ_WRITE) {
    				pInfo.writeTrans = -1;
    				if (commit) pInfo.hp.commit(tsId); // commit
    				else pInfo.hp.abort(tsId); // abort
    			}
    		}
    		tsMap.remove(tsId);
    	}
    }

    /**
     * Add a tuple to the specified table behalf of transaction tsId.  Will
     * acquire a write lock on the page the tuple is added to. May block if the lock cannot 
     * be acquired.
     * 
     * Marks any pages that were dirtied by the operation as dirty
     *
     * @param tsId the transaction adding the tuple
     * @param tableId the table to add the tuple to
     * @param t the tuple to add
     */
    public  void insertTuple(int tsId, int tableId, Tuple t)
        throws Exception {
    	HeapFile hf = getHf(tableId);
    	int pid = hf.getInsertHpId();
    	if (pid<0) throw new Exception("no enough page"); // 如果没过test要加逻辑 在heap file里面
    	HeapPage hp = this.getPage(tsId, tableId, pid, Permissions.READ_WRITE); // if unsuccessful will throw exception;
    	hp.tryInsert(t,tsId);
        // your code here
    }

    /**
     * Remove the specified tuple from the buffer pool.
     * Will acquire a write lock on the page the tuple is removed from. May block if
     * the lock cannot be acquired.
     *
     * Marks any pages that were dirtied by the operation as dirty.
     *
     * @param tsId the transaction adding the tuple.
     * @param tableId the ID of the table that contains the tuple to be deleted
     * @param t the tuple to add
     */
    public  void deleteTuple(int tsId, int tableId, Tuple t)
        throws Exception {
    	int pid =  t.getPid();
    	HeapPage hp = this.getPage(tsId, tableId, pid, Permissions.READ_WRITE);
    	hp.tryDelete(t, tsId);
        // your code here
    }

    private synchronized  void flushPage(int tableId, int pid) throws IOException {
        // your code here
    	String keyPair = "" + tableId + "," + pid;
    	if (pageMap.containsKey(keyPair)) {
    		int i = pageMap.get(keyPair);
    		pageInfo pInfo = pageInfoArray[i];
    		HeapPage hp = pInfo.hp;
    		if (hp.isDirty()) throw new IOException("the page is dirty");
    		pageInfoArray[i] = null;
    	} else throw new IOException("no such page");
    }

    /**
     * Discards a page from the buffer pool.
     * Flushes the page to disk to ensure dirty pages are updated on disk.
     */ 
    private synchronized  void evictPage() throws Exception {
        // your code here
    	for (int i = 0; i < pageInfoArray.length; i++) {
    		pageInfo pInfo = pageInfoArray[i];
    		HeapPage hp = pInfo.hp;
    		if (!hp.isDirty()) {
    			pageInfoArray[i] = null;
    		}
    	}
    }
    
    
    private class pageInfo {
    	private HeapPage hp;
    	private HashMap<Integer,Permissions> tramsMap; // tsId, perm
    	// there can be only one transaction write a page, -1 means no write lock,  
    	// if >= 0, means the transaction with that id is writing
    	int writeTrans = -1; 
    	
    	private pageInfo(int tsId ,HeapPage hp, Permissions perm) {
    		this.hp = hp;
    		this.tramsMap = new HashMap<Integer,Permissions>();
    		tramsMap.put(tsId, perm);
    	}
    }

}
