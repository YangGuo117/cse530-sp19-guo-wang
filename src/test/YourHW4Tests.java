package test;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

import hw1.Catalog;
import hw1.Database;
import hw1.HeapFile;
import hw1.HeapPage;
import hw1.IntField;
import hw1.StringField;
import hw1.Tuple;
import hw1.TupleDesc;
import hw4.BufferPool;
import hw4.Permissions;

public class YourHW4Tests {
	
	private Catalog c;
	private BufferPool bp;
	private HeapFile hf;
	private TupleDesc td;
	private int tid;
	
	@Before
	public void setup() {
		
		try {
			Files.copy(new File("testfiles/test.dat.bak").toPath(), new File("testfiles/test.dat").toPath(), StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			System.out.println("unable to copy files");
			e.printStackTrace();
		}
		
		c = Database.getCatalog();
		c.loadSchema("testfiles/test.txt");
		
		int tableId = c.getTableId("test");
		td = c.getTupleDesc(tableId);
		hf = c.getDbFile(tableId);
		
		bp = Database.getBufferPool();
		
		tid = c.getTableId("test");
	}

	@Test
	public void testHoldLock() throws Exception {
		// start a read transaction
		bp.getPage(0, tid, 0, Permissions.READ_ONLY);
		// the page in that table should be locked
		assertTrue(bp.holdsLock(0, tid, 0));
		// this lock should not belongs to other transaction
		assertFalse(bp.holdsLock(1, tid, 0));
		// other pages in the table should not be locked
		assertFalse(bp.holdsLock(0, tid, 1));
		
		bp.transactionComplete(0, true);
		// after commit this the page should be release
		assertFalse(bp.holdsLock(0, tid, 0));
	}
	@Test
	public void testDelete() throws Exception {
		Tuple t = new Tuple(td);
		t.setField(0, new IntField(new byte[] {0, 0, 0, (byte)131}));
		byte[] s = new byte[129];
		s[0] = 2;
		s[1] = 98;
		s[2] = 121;
		t.setField(1, new StringField(s));
		
		bp.getPage(0, tid, 0, Permissions.READ_WRITE); //acquire lock for the page
		bp.insertTuple(0, tid, t); //insert the tuple into the page
		bp.transactionComplete(0, true); //should flush the modified page
		
		//reset the buffer pool, get the page again, make sure data is there
		Database.resetBufferPool(BufferPool.DEFAULT_PAGES);
		HeapPage hp = bp.getPage(1, tid, 0, Permissions.READ_ONLY);
		bp.transactionComplete(1, true);
		Iterator<Tuple> it = hp.iterator();
		assertTrue(it.hasNext());
		it.next();
		assertTrue(it.hasNext());
		it.next();
		assertFalse(it.hasNext());
		
		
		// above is to test if insert is successful
		
		// below is to to delete and see if it works
		bp.getPage(2, tid, 0, Permissions.READ_WRITE); //acquire lock for the page
		bp.deleteTuple(2, tid, t); //delete
		bp.transactionComplete(2, true); //should flush the modified page
		
		//reset the buffer pool, get the page again, make sure data is there
		Database.resetBufferPool(BufferPool.DEFAULT_PAGES);
		HeapPage hpAfter = bp.getPage(3, tid, 0, Permissions.READ_ONLY);
		Iterator<Tuple> itAfter = hpAfter.iterator();
		bp.transactionComplete(3, true);
		assertTrue(itAfter.hasNext());
		itAfter.next();
		assertFalse(itAfter.hasNext());
	}

}
