package test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import hw1.Field;
import hw1.IntField;
import hw1.RelationalOperator;
import hw3.BPlusTree;
import hw3.Entry;
import hw3.InnerNode;
import hw3.LeafNode;
import hw3.Node;

public class YourHW3Tests {

	@Test
	public void testBorrow() {
		//Create a 3 x 3 tree, 
		//insert 1,2,3,4,5 and delete 2
		// it should looks like 3
		//                   13   45

		BPlusTree bt = new BPlusTree(3, 3);
		bt.insert(new Entry(new IntField(1), 0));
		bt.insert(new Entry(new IntField(2), 0));
		bt.insert(new Entry(new IntField(3), 0));
		bt.insert(new Entry(new IntField(4), 0));
		bt.insert(new Entry(new IntField(5), 0));

		bt.delete(new Entry(new IntField(2), 0));
		

		//verify root properties
		Node root = bt.getRoot();

		assertTrue(root.isLeafNode() == false);
		

		InnerNode in = (InnerNode)root;

		ArrayList<Field> k = in.getKeys();
		ArrayList<Node> c = in.getChildren();

		assertTrue(k.get(0).compare(RelationalOperator.EQ, new IntField(3)));
		
		//verify leaf properties
		assertTrue(c.get(0).isLeafNode() == true);
		assertTrue(c.get(1).isLeafNode() == true);
		LeafNode l = (LeafNode) c.get(0);
		LeafNode r = (LeafNode) c.get(1);
		assertTrue(l.getEntries().get(0).getField().compare(RelationalOperator.EQ, new IntField(1)));
		assertTrue(l.getEntries().get(1).getField().compare(RelationalOperator.EQ, new IntField(3)));
		assertTrue(r.getEntries().get(0).getField().compare(RelationalOperator.EQ, new IntField(4)));
		assertTrue(r.getEntries().get(1).getField().compare(RelationalOperator.EQ, new IntField(5)));
	}
	@Test
	public void testMerge() {
		//Create a 3 x 3 tree, 
		//insert 1,2,3,4,5 and delete 2 , 4
		// it should looks like 
		//                   135

		BPlusTree bt = new BPlusTree(3, 3);
		bt.insert(new Entry(new IntField(1), 0));
		bt.insert(new Entry(new IntField(2), 0));
		bt.insert(new Entry(new IntField(3), 0));
		bt.insert(new Entry(new IntField(4), 0));
		bt.insert(new Entry(new IntField(5), 0));

		bt.delete(new Entry(new IntField(2), 0));
		bt.delete(new Entry(new IntField(4), 0));
		
		//verify root properties
		Node root = bt.getRoot();

		assertTrue(root.isLeafNode() == true);
		LeafNode l = (LeafNode) root;
		assertTrue(l.getEntries().get(0).getField().compare(RelationalOperator.EQ, new IntField(1)));
		assertTrue(l.getEntries().get(1).getField().compare(RelationalOperator.EQ, new IntField(3)));
		assertTrue(l.getEntries().get(2).getField().compare(RelationalOperator.EQ, new IntField(5)));
	}

}
