package test;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import hw1.Catalog;
import hw1.Database;
import hw1.HeapFile;
import hw1.IntField;
import hw1.Tuple;
import hw1.TupleDesc;
import hw2.Query;
import hw2.Relation;

public class YourHW2Tests {

	private HeapFile testhf;
	private TupleDesc testtd;
	private HeapFile ahf;
	private TupleDesc atd;
	private Catalog c;

	@Before
	public void setup() {
		
		try {
			Files.copy(new File("testfiles/test.dat.bak").toPath(), new File("testfiles/test.dat").toPath(), StandardCopyOption.REPLACE_EXISTING);
			Files.copy(new File("testfiles/A.dat.bak").toPath(), new File("testfiles/A.dat").toPath(), StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			System.out.println("unable to copy files");
			e.printStackTrace();
		}
		
		c = Database.getCatalog();
		c.loadSchema("testfiles/test.txt");
		
		int tableId = c.getTableId("test");
		testtd = c.getTupleDesc(tableId);
		testhf = c.getDbFile(tableId);
		
		c = Database.getCatalog();
		c.loadSchema("testfiles/A.txt");
		
		tableId = c.getTableId("A");
		atd = c.getTupleDesc(tableId);
		ahf = c.getDbFile(tableId);
	}
	
	@Test
	public void testRename2() {
		Query q = new Query("SELECT a1 AS Alias1, a2 AS Alias2 FROM A");
		Relation r = q.execute();
		
		assertTrue("Select should not change the number of tuples", r.getTuples().size() == 8);
		assertTrue("Query does not add or remove column", r.getDesc().getSize() == 8);
		assertTrue("", r.getDesc().getFieldName(0).equals("Alias1"));
		assertTrue("", r.getDesc().getFieldName(1).equals("Alias2"));
	}
	@Test
	public void testRenameOneOf2() {
		Query q = new Query("SELECT a1 AS Alias1, a2 FROM A");
		Relation r = q.execute();
		
		assertTrue("Select should not change the number of tuples", r.getTuples().size() == 8);
		assertTrue("Query does not add or remove column", r.getDesc().getSize() == 8);
		assertTrue("", r.getDesc().getFieldName(0).equals("Alias1"));
		assertTrue("", r.getDesc().getFieldName(1).equals("a2"));
	}
	@Test
	public void testGroupByQ() {
		Query q = new Query("SELECT a1 as A1, SUM(a2) AS A2_SUM FROM A GROUP BY a1");
		Relation r = q.execute();
		assertTrue("", true);
		assertTrue("", r.getDesc().getFieldName(0).equals("A1"));
		assertTrue("", r.getDesc().getFieldName(1).equals("A2_SUM"));
		
		Query q2 = new Query("SELECT SUM(a1) AS A1_SUM, a2 AS A2 FROM A GROUP BY a2");
		Relation r2 = q2.execute();
		
		assertTrue("", r2.getDesc().getFieldName(0).equals("A1_SUM"));
		assertTrue("", r2.getDesc().getFieldName(1).equals("A2"));
		assertTrue("", true);
	}

}
