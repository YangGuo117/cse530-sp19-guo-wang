package hw2;

import java.util.ArrayList;

import hw1.Field;
import hw1.RelationalOperator;
import hw1.Tuple;
import hw1.TupleDesc;
import hw1.Type;

/**
 * This class provides methods to perform relational algebra operations. It will be used
 * to implement SQL queries.
 * @author Doug Shook
 *
 */
public class Relation {

	private ArrayList<Tuple> tuples;
	private TupleDesc td;
	
	public Relation(ArrayList<Tuple> l, TupleDesc td) {
		//your code here
		this.tuples = l;
		this.td = td;
	}
	
	/**
	 * This method performs a select operation on a relation
	 * @param field number (refer to TupleDesc) of the field to be compared, left side of comparison
	 * @param op the comparison operator
	 * @param operand a constant to be compared against the given column
	 * @return
	 */
	public Relation select(int field, RelationalOperator op, Field operand) {
		ArrayList<Tuple> newTp = new ArrayList<Tuple>();
		for (Tuple tp: tuples) {
			Field tempFd = tp.getField(field);
			if (tempFd.compare(op, operand)) newTp.add(tp);
		}
		//your code here
		return new Relation(newTp,td);
	}
	
	/**
	 * This method performs a rename operation on a relation
	 * @param fields the field numbers (refer to TupleDesc) of the fields to be renamed
	 * @param names a list of new names. The order of these names is the same as the order of field numbers in the field list
	 * @return
	 */
	public Relation rename(ArrayList<Integer> fields, ArrayList<String> names) {
//		TupleDesc newTD = new
		String[] fieldName = td.getFields();
		for (int i = 0; i < fields.size(); i++) {
			fieldName[fields.get(i)] = names.get(i);
		}
		//your code here
		TupleDesc newTd = new TupleDesc(td.getTypes(),fieldName);
		return new Relation(tuples,newTd);
	}
	
	/**
	 * This method performs a project operation on a relation
	 * @param fields a list of field numbers (refer to TupleDesc) that should be in the result
	 * @return
	 */
	public Relation project(ArrayList<Integer> fields) {
		ArrayList<Tuple> newTp = new ArrayList<Tuple>();
		int n = fields.size();
		String[] newFieldName = new String[n];
		Type[] newType = new Type[n];
		for (int i = 0; i < n; i++) {
			newFieldName[i] = td.getFieldName(fields.get(i));
			newType[i] = td.getType(fields.get(i));
		}
		TupleDesc newTd = new TupleDesc(newType,newFieldName);
		for (Tuple tp:this.tuples) {
			newTp.add(this.newTuple(tp, fields, newTd));
		}
		//your code here
		return new Relation(newTp,newTd);
	}
	private Tuple newTuple(Tuple oldTp, ArrayList<Integer> fields,TupleDesc newTd) {
		Tuple newTp = new Tuple(newTd);
		for (int i = 0; i < fields.size(); i++) newTp.setField(i, oldTp.getField(fields.get(i)));
		return newTp;
	}
	/**
	 * This method performs a join between this relation and a second relation.
	 * The resulting relation will contain all of the columns from both of the given relations,
	 * joined using the equality operator (=)
	 * @param other the relation to be joined
	 * @param field1 the field number (refer to TupleDesc) from this relation to be used in the join condition
	 * @param field2 the field number (refer to TupleDesc) from other to be used in the join condition
	 * @return
	 */
	public Relation join(Relation other, int field1, int field2) {
		ArrayList<Tuple> newTp = new ArrayList<Tuple>();
		int n1 = td.numFields();
		int n2 = other.getDesc().numFields();
		int n = n1 + n2;
		String[] newFieldName = new String[n];
		Type[] newType = new Type[n];
		for (int i = 0; i < n; i++) {
			if (i < n1) {
				newFieldName[i] = td.getFieldName(i);
				newType[i] = td.getType(i);
			} else {
				newFieldName[i] = other.getDesc().getFieldName(i-n1);
				newType[i] = other.getDesc().getType(i-n1);
			}
		}
		TupleDesc newTd = new TupleDesc(newType,newFieldName);
		for (Tuple tp1:this.tuples) {
			Field fd1 = tp1.getField(field1);
			for (Tuple tp2:other.getTuples()) {
				RelationalOperator op = RelationalOperator.EQ;
				Field fd2 = tp2.getField(field2);
				if (fd1.compare(op, fd2)) {
					newTp.add(this.combineTuples(tp1, tp2, newTd));
				}
			}
		}
		//your code here
		return new Relation(newTp, newTd);
	}
	private Tuple combineTuples(Tuple tp1,Tuple tp2,TupleDesc newTd) {
		Tuple newTp = new Tuple(newTd);
		int n1 = tp1.getDesc().numFields();
		int n2 = tp2.getDesc().numFields();
		int n = n1 + n2;
		for (int i = 0; i < n; i++) {
			if (i < n1) {
				newTp.setField(i, tp1.getField(i));
			} else {
				newTp.setField(i, tp2.getField(i-n1));
			}
		}
		return newTp;
	}
	
	/**
	 * Performs an aggregation operation on a relation. See the lab write up for details.
	 * @param op the aggregation operation to be performed
	 * @param groupBy whether or not a grouping should be performed
	 * @return
	 */
	public Relation aggregate(AggregateOperator op, boolean groupBy) {
		//your code here
		Aggregator agg = new Aggregator(op, groupBy, td);
		for (Tuple tp:this.tuples) agg.merge(tp);
		return new Relation(agg.getResults(), td);
	}
	
	public TupleDesc getDesc() {
		//your code here
		return this.td;
	}
	
	public ArrayList<Tuple> getTuples() {
		//your code here
		return this.tuples;
	}
	
	/**
	 * Returns a string representation of this relation. The string representation should
	 * first contain the TupleDesc, followed by each of the tuples in this relation
	 */
	public String toString() {
		String s = this.td.toString();
		for (Tuple tp:this.tuples) {
			s += tp.toString();
		}
		//your code here
		return s;
	}
}
