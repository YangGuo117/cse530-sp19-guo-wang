package hw2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import hw1.Field;
import hw1.IntField;
import hw1.RelationalOperator;
import hw1.StringField;
import hw1.Tuple;
import hw1.TupleDesc;
import hw1.Type;

/**
 * A class to perform various aggregations, by accepting one tuple at a time
 * @author Doug Shook
 *
 */
public class Aggregator {
	boolean groupBy;
	TupleDesc td;
	AggregateOperator o;
	HashMap<String,Integer> strMap;   // strvalue,index
	HashMap<Integer,Integer> intMap;  // intvalue,index
	ArrayList<Tuple> res;
	HashMap<Integer,Integer> avgBuffer;  // index,n
	HashMap<Integer,Integer> avgBuffer2; // index,sum
	
	
	public Aggregator(AggregateOperator o, boolean groupBy, TupleDesc td) {
		//your code here
		this.o = o;
		this.td = td;
		this.groupBy = groupBy;
		this.res = new ArrayList<Tuple>();
		this.intMap = new HashMap<Integer,Integer>();
		this.strMap = new HashMap<String,Integer>();
		this.avgBuffer = new HashMap<Integer,Integer>();
		this.avgBuffer2 = new HashMap<Integer,Integer>();
	}

	/**
	 * Merges the given tuple into the current aggregation
	 * @param t the tuple to be aggregated
	 */
	public void merge(Tuple t) {
		if (groupBy) {
			mergeGroupBy(t);
			return;
		}
		//your code here
		boolean isFirst = false;
		if (res.isEmpty()) {
			isFirst = true;
			res.add(t);
		}
		int curIndex = 0;
		RelationalOperator op;
		
		switch (o) {
		case AVG:
			if (td.getType(0) == Type.STRING) return;
			if (isFirst) {
				String[] fieldNames = td.getFields();
				Type[] types = td.getTypes();
				fieldNames[0] = fieldNames[0];
				types[0] = Type.INT;
				TupleDesc newTd = new TupleDesc(types, fieldNames);
				Tuple newTp = new Tuple(newTd);
				newTp.setField(0, t.getField(0));
				res.set(curIndex, newTp);
				avgBuffer.put(curIndex, 1);
				IntField tempFd = (IntField) t.getField(0);
				avgBuffer2.put(curIndex, tempFd.getValue());
				break;
			} else {
				IntField tempFd2 = (IntField) t.getField(0);
				int n = avgBuffer.get(curIndex);
				int prevSum = avgBuffer2.get(curIndex);
				int num = (prevSum + tempFd2.getValue())/(n + 1);
				res.get(curIndex).setField(0, new IntField(num));
				avgBuffer.put(curIndex, n + 1);
				avgBuffer2.put(curIndex, prevSum + tempFd2.getValue());
				break;
			}
		case MAX:
			op = RelationalOperator.GT;
			if (t.getField(0).compare(op, res.get(0).getField(0))) {
				res.set(0, t);
			}
			break;
		case MIN:
			op = RelationalOperator.LT;
			if (t.getField(0).compare(op, res.get(0).getField(0))) {
				res.set(0, t);
			}
			break;
		case COUNT:
			if (isFirst) {
				String[] fieldNames = td.getFields();
				Type[] types = td.getTypes();
				fieldNames[0] = fieldNames[0];
				types[0] = Type.INT;
				TupleDesc newTd = new TupleDesc(types, fieldNames);
				Tuple newTp = new Tuple(newTd);
				newTp.setField(0, new IntField(1));
				res.set(curIndex, newTp);
			} else {
				IntField tempFd = (IntField) res.get(curIndex).getField(0);
				res.get(curIndex).setField(0, new IntField(tempFd.getValue() + 1));
			}
			break;
		case SUM:
			if (td.getType(0) == Type.STRING) return;
			if (isFirst) {
				String[] fieldNames = td.getFields();
				Type[] types = td.getTypes();
				fieldNames[0] = fieldNames[0];
				types[0] = Type.INT;
				TupleDesc newTd = new TupleDesc(types, fieldNames);
				Tuple newTp = new Tuple(newTd);
				newTp.setField(0, t.getField(0));
				res.set(curIndex, newTp);
			} else {
				IntField tempFd1 = (IntField) res.get(curIndex).getField(0);
				IntField tempFd2 = (IntField) t.getField(0);
				res.get(curIndex).setField(0, new IntField(tempFd1.getValue() + tempFd2.getValue()));
			}
			break;
		default:
			return;
		}
	}
	
	private void mergeGroupBy(Tuple t) {
		int curIndex;
		boolean isFirst = false;
		if (t.getField(0).getType() == Type.INT) {
			IntField curFd1 = (IntField) t.getField(0);
			int keyVal = curFd1.getValue();
			if (!intMap.containsKey(keyVal)) {
				curIndex = res.size();
				intMap.put(keyVal, curIndex);
				res.add(t);
				isFirst = true;
			} else {
				curIndex = intMap.get(keyVal);
			}
		} else {
			StringField curFd1 = (StringField) t.getField(0);
			String keyVal = curFd1.getValue();
			if (!strMap.containsKey(keyVal)) {
				curIndex = res.size();
				strMap.put(keyVal, curIndex);
				res.add(t);
				isFirst = true;
			} else {
				curIndex = strMap.get(keyVal);
			}
		}
		RelationalOperator op;
		Tuple curTp = res.get(curIndex);
		
		switch (o) {
		case AVG:
			if (td.getType(1) == Type.STRING) return;
			if (isFirst) {
				String[] fieldNames = td.getFields();
				Type[] types = td.getTypes();
				fieldNames[1] = fieldNames[1];
				types[1] = Type.INT;
				TupleDesc newTd = new TupleDesc(types, fieldNames);
				Tuple newTp = new Tuple(newTd);
				newTp.setField(0, t.getField(0));
				newTp.setField(1, t.getField(1));
				res.set(curIndex, newTp);
				avgBuffer.put(curIndex, 1);
				IntField tempFd = (IntField) t.getField(1);
				avgBuffer2.put(curIndex, tempFd.getValue());
			} else {
//				IntField tempFd1 = (IntField) res.get(curIndex).getField(1);
				IntField tempFd2 = (IntField) t.getField(1);
				int n = avgBuffer.get(curIndex);
				int prevSum = avgBuffer2.get(curIndex);
				int num = (prevSum + tempFd2.getValue())/(n + 1);
				res.get(curIndex).setField(1, new IntField(num));
				avgBuffer.put(curIndex, n + 1);
				avgBuffer2.put(curIndex, prevSum + tempFd2.getValue());
			}
			break;
		case MAX:
			op = RelationalOperator.GT;
			if (t.getField(1).compare(op, curTp.getField(1))) {
				res.set(curIndex, t);
			}
			break;
		case MIN:
			op = RelationalOperator.LT;
			if (t.getField(1).compare(op, curTp.getField(1))) {
				res.set(curIndex, t);
			}
			break;
		case COUNT:
			if (isFirst) {
				String[] fieldNames = td.getFields();
				Type[] types = td.getTypes();
				fieldNames[1] = fieldNames[1];
				types[1] = Type.INT;
				TupleDesc newTd = new TupleDesc(types, fieldNames);
				Tuple newTp = new Tuple(newTd);
				newTp.setField(0, t.getField(0));
				newTp.setField(1, new IntField(1));
				res.set(curIndex, newTp);
			} else {
				IntField tempFd = (IntField) res.get(curIndex).getField(1);
				res.get(curIndex).setField(1, new IntField(tempFd.getValue() + 1));
			}
			break;
		case SUM:
			if (td.getType(1) == Type.STRING) return;
			if (isFirst) {
				String[] fieldNames = td.getFields();
				Type[] types = td.getTypes();
				fieldNames[1] = fieldNames[1];
				types[1] = Type.INT;
				TupleDesc newTd = new TupleDesc(types, fieldNames);
				Tuple newTp = new Tuple(newTd);
				newTp.setField(0, t.getField(0));
				newTp.setField(1, t.getField(1));
				res.set(curIndex, newTp);
			} else {
				IntField tempFd1 = (IntField) res.get(curIndex).getField(1);
				IntField tempFd2 = (IntField) t.getField(1);
				res.get(curIndex).setField(1, new IntField(tempFd1.getValue() + tempFd2.getValue()));
			}
			break;
		default:
			return;
		}
	}
	
	/**
	 * Returns the result of the aggregation
	 * @return a list containing the tuples after aggregation
	 */
	public ArrayList<Tuple> getResults() {
		int index = 0;
		if (groupBy) {
			index = 1;
		}
		switch (o) {
		case AVG:
			if (res.get(0).getField(index).getType() == Type.STRING) {
				return new ArrayList<Tuple>();
			} else {
				return res;
			}
		case MAX: 
			return res;
		case MIN:
			return res;
		case COUNT:
			return res;
		case SUM:
			if (res.get(0).getField(index).getType() == Type.STRING) {
				return new ArrayList<Tuple>();
			} else {
				return res;
			}
		default:
			return new ArrayList<Tuple>();
		}
	}

}
