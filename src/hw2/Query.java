package hw2;

import java.util.ArrayList;
import java.util.List;

import hw1.Catalog;
import hw1.Database;
import hw1.HeapFile;
import hw1.IntField;
import hw1.Field;
import hw1.RelationalOperator;
import hw1.StringField;
import hw1.Type;
import hw2.AggregateExpressionVisitor;
import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.expression.Alias;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.ExpressionVisitor;
import net.sf.jsqlparser.expression.operators.relational.EqualsTo;
import net.sf.jsqlparser.parser.*;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.*;
import net.sf.jsqlparser.statement.select.FromItem;
import net.sf.jsqlparser.statement.select.Join;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;
import net.sf.jsqlparser.statement.select.SelectBody;
import net.sf.jsqlparser.statement.select.SelectExpressionItem;
import net.sf.jsqlparser.statement.select.SelectItem;

public class Query {

	private String q;
	
	public Query(String q) {
		this.q = q;
	}
	
	public Relation execute()  {
		Statement statement = null;
		try {
			statement = CCJSqlParserUtil.parse(q);
		} catch (JSQLParserException e) {
			System.out.println("Unable to parse query");
			e.printStackTrace();
		}
		Select selectStatement = (Select) statement;
		PlainSelect sb = (PlainSelect)selectStatement.getSelectBody();
		
		
		// ==============  from
		String fromTableName = sb.getFromItem().toString();
		Relation fromTbR = this.getRelation(fromTableName); // relation of 'from' table 
		
		System.out.println("============================");
//		System.out.println(sb.getFromItem().toString());
		System.out.println(sb.toString());
//		System.out.println(statement.toString());
//		System.out.println("join");
//		System.out.println(sb.getJoins().toString());
//		
		// ============== join
		Relation res = fromTbR;
		List<Join> joinList = sb.getJoins();
		if (joinList!=null) {
			for (Join join: joinList) {
				Expression joinOnExp = join.getOnExpression();
				String curName = join.getRightItem().toString();
				Relation curTbR = this.getRelation(curName);
				WhereExpressionVisitor wev = new WhereExpressionVisitor();
				joinOnExp.accept(wev);
				StringField curField = (StringField) wev.getRight();
				String[] rightInfo = curField.getValue().split("\\."); // table name, field name
				
				int rightFieldId;
				int leftFieldId;
				if (rightInfo.length != 2) continue;
				if (rightInfo[0].equals(curName.toLowerCase()) || rightInfo[0].equals(curName.toUpperCase())) { // FROM test(res) JOIN A(cur) ON test.c1 = a.a1
					rightFieldId = curTbR.getDesc().nameToId(rightInfo[1]);
					leftFieldId = res.getDesc().nameToId(wev.getLeft());
					res = res.join(curTbR, leftFieldId, rightFieldId);
				} else { // FROM A(res) JOIN test(cur) ON test.c1 = a.a1
					rightFieldId = res.getDesc().nameToId(rightInfo[1]);
					leftFieldId = curTbR.getDesc().nameToId(wev.getLeft());
					res = res.join(curTbR, rightFieldId, leftFieldId);
				}
			}
		}
		// ============== where
		Expression whereExp = sb.getWhere();
		if (whereExp != null) {
			WhereExpressionVisitor wev = new WhereExpressionVisitor();
			whereExp.accept(wev);
			RelationalOperator op = wev.getOp();
			String leftStr = wev.getLeft();
			int leftTbId = res.getDesc().nameToId(leftStr);
			Field curField =  wev.getRight();
			res = res.select(leftTbId, op, curField);
		}
		
		// ============== select
		List<SelectItem> selectList = sb.getSelectItems();
		ArrayList<String> aliasList = new ArrayList<String>();
		ArrayList<String> aliasOldName = new ArrayList<String>();
		
		ArrayList<String> groupName = new ArrayList<String>();
		ArrayList<AggregateOperator> operations = new ArrayList<AggregateOperator>();
		if (selectList != null) {
			ArrayList<Integer> fields = new ArrayList<Integer>();
			ColumnVisitor cv = new ColumnVisitor();
			for (SelectItem sItem:selectList) {
				sItem.accept(cv);
				String curName = cv.getColumn();
				AggregateOperator op = cv.getOp();
				if (op == null && curName.equals("*")) break; // select *
				// prepare for rename
				SelectExpressionItem sExp = (SelectExpressionItem) sItem;
				Alias alias = sExp.getAlias();
				if (alias != null) {
					String[] aliasInfo = alias.toString().split(" ");
					aliasList.add(aliasInfo[2]);
					aliasOldName.add(curName);
				}
				if (op == AggregateOperator.COUNT && curName.equals("*")) {// count(*)
					fields.add(0);
					res = res.project(fields).aggregate(op, false); //count the field 0
					fields.clear();
					break;
				} else if(op != null) { // it means it need aggregate
					groupName.add(curName);
					operations.add(op);
				}
				fields.add(res.getDesc().nameToId(curName));
			}
			if (!fields.isEmpty()) res = res.project(fields);
		}
		// ============== group by
		Boolean isGB = false;
		List<Expression> groupList = sb.getGroupByColumnReferences();
		if (groupList != null) {
			for (Expression exp : groupList) {
				AggregateExpressionVisitor aev = new AggregateExpressionVisitor();
				exp.accept(aev);
				String columnName = aev.getColumn();
				ArrayList<Integer> tempField = new ArrayList<Integer>();
				if (res.getDesc().numFields() == 2) { // only in this situation can we use group by.
					if (!operations.isEmpty()) {
						AggregateOperator op = operations.get(0);
						int groupField = res.getDesc().nameToId(columnName);  // 1,0
						int otherField = (groupField == 0) ? 1 : 0;           // 0,1
						tempField.add(groupField);
						tempField.add(otherField);
						res = res.project(tempField);
						res = res.aggregate(op, true);
						isGB = true;
						if (groupField == 1) res = res.project(tempField);
					}
				}
			}
		}
		// ============== rename
		ArrayList<Integer> aliasIndex = new ArrayList<Integer>();
		for (int i = 0; i < aliasOldName.size(); i++) aliasIndex.add(res.getDesc().nameToId(aliasOldName.get(i)));
		if (aliasIndex.size() > 0)	res = res.rename(aliasIndex, aliasList);
		
		// ============== re do operator of select, if there are any
		// this part only handles queries like Select sum(a1) from A
		// since if aggregate and not group by, the field number can only be 1.
		// we can only handle 1 column in this case.
		if (operations.size() == 1 && res.getDesc().numFields() == 1 && !isGB) { 
			AggregateOperator op = operations.get(0);
			res = res.aggregate(op, false);
		}
		return res;
	}
	
	private Relation getRelation(String tableName) {
		String fileDir = "testfiles/"+ tableName + ".txt";
		Catalog c = Database.getCatalog();
		c.loadSchema(fileDir);
		
		int tableId = c.getTableId(tableName);
		HeapFile hf = c.getDbFile(tableId);
		return new Relation(hf.getAllTuples(), hf.getTupleDesc());
	}
	 
}
