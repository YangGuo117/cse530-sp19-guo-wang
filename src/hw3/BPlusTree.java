package hw3;


import java.util.ArrayList;
import hw1.RelationalOperator;
import hw1.Field;

public class BPlusTree {
	private Node rt;
	private int pInner;
	private int pLeaf;
    
    public BPlusTree(int pInner, int pLeaf) {
    	//your code here 
    	LeafNode lf = new LeafNode(pLeaf);
    	this.rt = lf;
    	this.pInner = pInner;
    	this.pLeaf = pLeaf;
    }
    public LeafNode search(Field f) {
    	LeafNode res = (LeafNode) searchLeaf(f);
    	for (Entry Ent:res.getEntries()) {
    		if (f.compare(RelationalOperator.EQ, Ent.getField())) return res;
    	}
    	return null;
    }
    private LeafNode searchLeaf(Field f) {
    	//your code here
    	Node curNode = this.rt;
    	while (!curNode.isLeafNode()) { // while current node is not leaf
    		curNode = (InnerNode) curNode;
    		ArrayList<Field> keys = ((InnerNode) curNode).getKeys();
    		ArrayList<Node> childs = ((InnerNode) curNode).getChildren();
    		Boolean isFind = false;
    		for (int i = 0; i < keys.size(); i++) {
    			if (f.compare(RelationalOperator.LTE, keys.get(i))) { // f <= tempKey
    				childs.get(i).setParent((InnerNode) curNode);
    				curNode = childs.get(i);
    				isFind = true;
    				break;
    			}
    		}
    		if (isFind) continue;
    		childs.get(keys.size()).setParent((InnerNode) curNode);
    		curNode = childs.get(keys.size()); // if all keys < f, this line will run
    	}
    	return (LeafNode) curNode;
    }
    
    public void insert(Entry e) {
    	//your code here
    	LeafNode curLeaf = searchLeaf(e.getField());
    	if (!curLeaf.isFull()) {
    		// add entry
    		ArrayList<Entry> curEntry = curLeaf.getEntries();
    		// if null or larger than the max key
    		if (curEntry.size() == 0 || e.getField().compare(RelationalOperator.GT, curEntry.get(curEntry.size()-1).getField())) {
    			curEntry.add(e);
    		} else {
    			for (int i = 0; i < curEntry.size(); i++) {
        			if (e.getField().compare(RelationalOperator.LTE, curEntry.get(i).getField())) {
        				curEntry.add(i, e);
        				break;
        			}
        		}
    		}
    		return;
    	} else { // if is full and need split
    		ArrayList<Entry> curEntry = (ArrayList<Entry>) curLeaf.getEntries().clone();
    		if (curEntry.size() == 0 || e.getField().compare(RelationalOperator.GT, curEntry.get(curEntry.size()-1).getField())) {
    			curEntry.add(e);
    		} else {
	    		for (int i = 0; i < curEntry.size(); i++) {
	    			if (e.getField().compare(RelationalOperator.LTE, curEntry.get(i).getField())) {
	    				curEntry.add(i, e);
	    				break;
	    			}
	    		}
    		}
    		int len = curEntry.size();
        	int half = (len - 1) / 2;
        	LeafNode l = new LeafNode(this.pLeaf);
        	LeafNode r = new LeafNode(this.pLeaf);
        	ArrayList<Entry> rEntry = r.getEntries();
        	ArrayList<Entry> lEntry = l.getEntries();
        	Field fd = curEntry.get(half).getField();
        	for (int i = 0; i <= half; i++) {
        		lEntry.add(curEntry.get(i));
        	}
        	for (int ii = half+1; ii < len; ii++) {
        		rEntry.add(curEntry.get(ii));
        	}
        	//
//        	l.setParent(curLeaf.getParent());
//        	r.setParent(curLeaf.getParent());
//        	insertToNode(curLeaf.getParent(),fd,l,r);
        	InnerNode handledNode = insertToNode(curLeaf.getParent(),fd,l,r);
        	l.setParent(handledNode);
        	r.setParent(handledNode);
    	}
    }
    
    private InnerNode insertToNode(InnerNode curNode,Field f,Node l,Node r) {
    	//if curNode = null
    	if (curNode == null) {
    		InnerNode newNode = new InnerNode(this.pInner);
//    		l.setParent(newNode);
//    		r.setParent(newNode);
    		newNode.getChildren().add(l);
    		newNode.getChildren().add(r);
    		newNode.getKeys().add(f);
    		this.rt = newNode;
    		return newNode;
    	}
    	
    	
    	// if curNode is not full
    	if (curNode!= null && !curNode.isFull()) {
//    		l.setParent(curNode);
//    		r.setParent(curNode);
    		ArrayList<Field> pKeys = curNode.getKeys();
    		ArrayList<Node> pChilds = curNode.getChildren();
    		int lastIndex = pKeys.size() - 1;
    		if (f.compare(RelationalOperator.GT, pKeys.get(lastIndex))) {
    			pChilds.set(lastIndex,l);
				pChilds.add(r);
				pKeys.add(f);
    		} else {
    			for (int i = 0; i < pKeys.size(); i++) {
        			if (f.compare(RelationalOperator.LTE, pKeys.get(i))) {
        				pChilds.set(i,l);
        				pChilds.add(i+1, r);
        				pKeys.add(i, f);
        				break;
        			}
        		}
    		}
    		return curNode;
    	}
    	
    	// if curNode is full
    	ArrayList<Field> pKeys = (ArrayList<Field>) curNode.getKeys().clone();
		ArrayList<Node> pChilds = (ArrayList<Node>) curNode.getChildren().clone();
		int lastIndex = pKeys.size() - 1;
		if (f.compare(RelationalOperator.GT, pKeys.get(lastIndex))) {
			pChilds.set(lastIndex,l);
			pChilds.add(r);
			pKeys.add(f);
		} else {
			for (int i = 0; i < pKeys.size(); i++) {
				if (f.compare(RelationalOperator.LTE, pKeys.get(i))) {
					pChilds.set(i,l);
					pChilds.add(i+1, r);
					pKeys.add(i, f);
					break;
				}
			}
		}
		InnerNode newL = new InnerNode(this.pInner);
		InnerNode newR = new InnerNode(this.pInner);
		ArrayList<Field> LKeys = newL.getKeys();
		ArrayList<Node> LChilds = newL.getChildren();
		ArrayList<Field> RKeys = newR.getKeys();
		ArrayList<Node> RChilds = newR.getChildren();
		int len = pKeys.size();
		int half = (len - 1) / 2;
		Field fd = pKeys.get(half);
		for (int i = 0; i < half; i++) {
			LKeys.add(pKeys.get(i));
    	}
    	for (int i = half+1; i < len; i++) {
    		RKeys.add(pKeys.get(i));
    	}
    	for (int i = 0; i < pChilds.size();i++) {
    		if (i <= LKeys.size()) {
    			LChilds.add(pChilds.get(i));
    		} else {
    			RChilds.add(pChilds.get(i));
    		}
    	}
//    	newL.setParent(curNode.getParent());
//    	newR.setParent(curNode.getParent());
    	
//    	Boolean isNull = curNode.getParent()==null;
//    	Boolean parenfFull = curNode.getParent().isFull();
    	InnerNode handledNode = insertToNode(curNode.getParent(),fd,newL,newR);
    	newL.setParent(handledNode);
    	newR.setParent(handledNode);
    	
    	if (f.compare(RelationalOperator.LTE, fd)) return newL;
    	else return newR;
    }
    
    public void delete(Entry e) {
    	//your code here
    	LeafNode curLeaf = searchLeaf(e.getField());
    	deleteLeaf(curLeaf,e.getField());
    }
    
    private void deleteLeaf(LeafNode lf, Field f) {
    	ArrayList<Entry> curEntry = lf.getEntries();
    	// delete entry in leaf
    	for (int i = 0; i < curEntry.size(); i++) {
    		if (f.compare(RelationalOperator.EQ, curEntry.get(i).getField())) {
    			curEntry.remove(i);
    		}
    	}
    	// if it is still half full, then we are done, so as to if we did not found it
    	if (lf.isHalfFull()) return;
    	// if not half full
    	
    	// try borrow
    	InnerNode pr = lf.getParent();
    	if (pr == null) return; // there no ware to borrow;
    	
    	ArrayList<Field> pKeys = pr.getKeys();
		ArrayList<Node> pChilds = pr.getChildren();
		int cldIndex = -1;
		for (int i = 0; i < pKeys.size(); i++) {
			if (f.compare(RelationalOperator.LTE, pKeys.get(i))) {
				cldIndex = i;
				break;
			}
		}
		if (cldIndex < 0) cldIndex = pChilds.size() - 1;
		// try borrow left
		if (cldIndex - 1 >= 0 && cldIndex - 1 < pChilds.size()) {
			LeafNode lCld = (LeafNode) pChilds.get(cldIndex-1);
			if (lCld.canBorrow()) {
				ArrayList<Entry> lEntry = lCld.getEntries();
				Entry l = lEntry.get(lEntry.size() - 1); //left
				lEntry.remove(lEntry.size() - 1); // remove left max
				int targetIndex = -1;
				for (int i = 0; i < pKeys.size(); i++) {
					if (l.getField().compare(RelationalOperator.LTE, pKeys.get(i))) {
						targetIndex = i;
						break;
					}
				}
				if (targetIndex < 0) targetIndex = pKeys.size()-1;
				pKeys.set(targetIndex, lEntry.get(lEntry.size() - 1).getField()); //update parent;
				curEntry.add(0, l);
				return;
			}
		}
		
		// try borrow right
		if (cldIndex + 1 >= 0 && cldIndex + 1 < pChilds.size()) {
			LeafNode rCld = (LeafNode) pChilds.get(cldIndex+1);
			if (rCld.canBorrow()) {
				ArrayList<Entry> rEntry = rCld.getEntries();
				Entry r = rEntry.get(rEntry.size()-1);
				rEntry.remove(0); // remove left min
				int targetIndex = -1;
				for (int i = 0; i < pKeys.size(); i++) { // find target of self
					if (f.compare(RelationalOperator.LTE, pKeys.get(i))) {
						targetIndex = i;
						break;
					}
				}
				if (targetIndex < 0) targetIndex = pKeys.size()-1;
				pKeys.set(targetIndex, r.getField());
				curEntry.add(r);
				return;
			}
		}
		
		// if code comes to hear means this leaf can not borrow from left or right
		// and it must have a parent
		
		// merge left if this is not the first child
		if (cldIndex > 0) {
			LeafNode lCld = (LeafNode) pChilds.get(cldIndex-1);
			ArrayList<Entry> lEntry = lCld.getEntries();
			// delete parent key where left is found
			InnerNode newPrent = deleteInner(pr,f);
			lEntry.addAll(curEntry);
			lCld.setParent(newPrent);
			if (newPrent == null) this.rt = lCld;
		}
		else { // this is first child, merge right
			LeafNode rCld = (LeafNode) pChilds.get(cldIndex+1);
			ArrayList<Entry> rEntry = rCld.getEntries();
			// delete parent key where this leaf is found
			InnerNode newPrent = deleteInner(pr,f);
			ArrayList<Entry> tempEntry = (ArrayList<Entry>) curEntry.clone();
			tempEntry.addAll(rEntry);
			rEntry.clear();
			rEntry.addAll(tempEntry);
			rCld.setParent(newPrent);
			if (newPrent == null) this.rt = rCld;
		}
    }
    
    
    private InnerNode deleteInner(InnerNode curNode, Field f) {
    	ArrayList<Field> curKeys = curNode.getKeys();
		ArrayList<Node> curChilds = curNode.getChildren();
    	
    	
    	// if this node is delete able ,then just delete
    	int keyIndex = -1;
    	int cldIndex = -1;
    	for (int i = 0; i < curKeys.size(); i++) {
    		if (f.compare(RelationalOperator.LTE,curKeys.get(i))) {
    			keyIndex = i;
    			cldIndex = i;
    			break;
    		}
    	}
    	if (keyIndex < 0) {
    		keyIndex = curKeys.size()-1;
    		cldIndex = curKeys.size();
    	}
    	Field fd = curKeys.get(keyIndex);
    	// delete
    	curKeys.remove(keyIndex);
    	curChilds.remove(cldIndex);
    	
    	// if after delete, it is still half full, we are done
    	if (curNode.isHalfFull()) return curNode;
    	
    	// if it is not half full
    	InnerNode pr = curNode.getParent();
    	
    	// if parent is null
    	if (pr == null) {
    		// if no keys left
    		if (curKeys.size() <= 0) return null;
    		// if still keys left
    		return curNode;
    	}
    	//  if parent is not null
    	// first check out can we borrow from left and right
    	// try borrow
    	ArrayList<Field> pKeys = pr.getKeys();
		ArrayList<Node> pChilds = pr.getChildren();
		int pcldIndex = -1;
		for (int i = 0; i < pKeys.size(); i++) {
			if (f.compare(RelationalOperator.LTE, pKeys.get(i))) {
				pcldIndex = i;
				break;
			}
		}
		if (pcldIndex < 0) pcldIndex = pKeys.size();
    	
    	//borrow left
		if (pcldIndex - 1 >= 0 && pcldIndex - 1 < pChilds.size()) {
			InnerNode lCld = (InnerNode) pChilds.get(pcldIndex-1);
			if (lCld.canBorrow()) {
				// get key can child be borrowed from left
				ArrayList<Field> lKeys = lCld.getKeys();
				ArrayList<Node> lChilds = lCld.getChildren();
				Field lKey = lKeys.get(lKeys.size()-1);
				Node lChild = lChilds.get(lChilds.size()-1);
				// delete left
				lKeys.remove(lKeys.size()-1);
				lChilds.remove(lChilds.size()-1);
				// find index in parent
				int targetIndex = -1;
				for (int i = 0; i < pKeys.size(); i++) {
					if (lKey.compare(RelationalOperator.LTE, pKeys.get(i))) {
						targetIndex = i;
						break;
					}
				}
				//update parent;
				Field tempFd = null;
				if (targetIndex < 0) {
					tempFd = pKeys.get(pKeys.size()-1);
					pKeys.set(pKeys.size()-1, lKey);
//					pChilds.set(pChilds.size()-1, lChild);
				} else {
					tempFd = pKeys.get(targetIndex);
					pKeys.set(targetIndex, lKey); 
//					pChilds.set(targetIndex, lChild); 
				}
				// update self
				curKeys.add(0,tempFd);
				curChilds.add(0, lChild);
				return curNode;
			}
		}
		
		// try borrow right
		if (pcldIndex + 1 >= 0 && pcldIndex + 1 < pChilds.size()) {
			InnerNode rCld = (InnerNode) pChilds.get(pcldIndex+1);
			if (rCld.canBorrow()) {
				// find right
				ArrayList<Field> rKeys = rCld.getKeys();
				ArrayList<Node> rChilds = rCld.getChildren();
				Field rKey = rKeys.get(0);
				Node rChild = rChilds.get(0);
				// delete right min
				rKeys.remove(0); 
				rChilds.remove(0);
				// find index in parent
				int targetIndex = -1;
				for (int i = 0; i < pKeys.size(); i++) {
					if (rKey.compare(RelationalOperator.LTE, pKeys.get(i))) {
						targetIndex = i;
						break;
					}
				}
				// update parent
				if (targetIndex < 0) {
					pKeys.set(pKeys.size()-1, rKey);
//					pChilds.set(pChilds.size()-1, rChild);
				} else {
					pKeys.set(targetIndex, rKey); 
//					pChilds.set(targetIndex, rChild); 
				}
				// update self
				curKeys.add(rKey);
				curChilds.add(rChild);
				return curNode;
			}
		}
    	// if come to this line means it can not borrow from both side
		// so we have to merge
		if (pcldIndex > 0) { // not first child merge left
			Field tempFd = pKeys.get(0);
			InnerNode lCld = (InnerNode) pChilds.get(pcldIndex-1);
			ArrayList<Field> lKeys = lCld.getKeys();
			ArrayList<Node> lChilds = lCld.getChildren();
			// delete parent where left node if is found
			InnerNode newPrent = deleteInner(pr,lKeys.get(0));
			if (newPrent == null) {
				this.rt = lCld;
				curKeys.add(tempFd);
			}
			lKeys.addAll(curKeys);
			lChilds.addAll(curChilds);
			lCld.setParent(newPrent);
			return lCld;
		}
		else { // this is first child, merge right
			Field tempFd = pKeys.get(0);
			InnerNode rCld = (InnerNode) pChilds.get(cldIndex+1);
			ArrayList<Field> rKeys = rCld.getKeys();
			ArrayList<Node> rChilds = rCld.getChildren();
			// delete in parent where this node is found
			InnerNode newPrent = deleteInner(pr,f);
			ArrayList<Field> tempKey = (ArrayList<Field>) curKeys.clone();
			ArrayList<Node> tempChilds = (ArrayList<Node>) curChilds.clone();
			if (newPrent == null) {
				this.rt = rCld;
				tempKey.add(tempFd);
			}
			tempKey.addAll(rKeys);
			tempChilds.addAll(rChilds);
			rKeys.clear();
			rKeys.addAll(tempKey);
			rChilds.clear();
			rChilds.addAll(tempChilds);
			rCld.setParent(newPrent);
			return rCld;
		}
    }
    
    
    
    
    
    
    public Node getRoot() {
    	//your code here
    	return this.rt;
    }
    


	
}
