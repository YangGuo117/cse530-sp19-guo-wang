package hw3;

import java.util.ArrayList;

public class LeafNode implements Node {
	private int dg;
	private ArrayList<Entry> Entries;
	private InnerNode parent = null;
	
	public LeafNode(int degree) {
		//your code here
		this.dg = degree;
		this.Entries = new ArrayList<Entry>();
	}
	
	public ArrayList<Entry> getEntries() {
		//your code here
		return Entries;
	}
	
	public Boolean isHalfFull() {
		return Entries.size() >= Math.ceil(1.0 * dg / 2);
	}
	public Boolean canBorrow() {
		return Entries.size() - 1 >= Math.ceil(1.0 * dg / 2);
	}
	
	public Boolean isFull() {
		return Entries.size() == dg;
	}
	
	public InnerNode getParent() {
		return this.parent;
	}
	
	public void setParent(InnerNode p) {
		this.parent = p;
	}
	
	public int getDegree() {
		//your code here
		return dg;
	}
	
	public boolean isLeafNode() {
		return true;
	}

}