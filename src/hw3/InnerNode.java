package hw3;

import java.util.ArrayList;

import hw1.Field;

public class InnerNode implements Node {
	private int dg;
	private InnerNode parent = null;
	private ArrayList<Field> keys;
	private ArrayList<Node> childs;
	
	public InnerNode(int degree) {
		this.dg = degree;
		this.keys = new ArrayList<Field>();
		this.childs = new ArrayList<Node>();
		//your code here
	}
	
	public ArrayList<Field> getKeys() {
		//your code here
		return keys;
	}
	public void setKeys(ArrayList<Field> fds) {
		//your code here
		this.keys = fds;
	}
	
	public ArrayList<Node> getChildren() {
		//your code here
		return childs;
	}
	public void setChildren(ArrayList<Node> clds) {
		//your code here
		this.childs = clds;
	}
	
	public Boolean isHalfFull() {
		return keys.size() >= Math.ceil(1.0 * (dg-1) / 2);
	}
	public Boolean canBorrow() {
		return keys.size() - 1 >= Math.ceil(1.0 * (dg-1) / 2);
	}
	
	public Boolean isFull() {
		return keys.size() == dg-1;
	}
	
	public InnerNode getParent() {
		return this.parent;
	}
	
	public void setParent(InnerNode p) {
		this.parent = p;
	}

	public int getDegree() {
		//your code here
		return 0;
	}
	
	public boolean isLeafNode() {
		return false;
	}

}